#!/bin/bash

docker tag $LOGNAME/node-svc gcr.io/$GOOGLE_CLOUD_PROJECT/node-svc:v1
docker push gcr.io/$GOOGLE_CLOUD_PROJECT/node-svc:v1
